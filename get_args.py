import argparse


def get_args():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('img_file', type = str, help = 'path to the folder of training images')
    parser.add_argument('model_path', type = str, help = 'load the checkpoint')
    parser.add_argument('--top_k', type = int, default = 3, help = 'top number of predictions')
    parser.add_argument('--category_names', type = str, default = 'label_map.json', help = 'load the category key')
    
    return parser.parse_args()
